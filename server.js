var http = require('http');
var url = require('url');
var fs = require('fs');
var path = require('path');
var mime = require('mime');

function Server(){
    this.server = null;
        this.inGame = false;

        this.createServer();
        this.initMaps();

        this.idCount = 0;
        this.idPlayerCount = 0;
        this.units = [];
        this.players = [];
        this.nbConnected = 0;
        this.ready = 0;
        this.choosenMap = 0;

        this.setIOSockets();
}

Server.prototype.createServer = function(){
        this.server = http.createServer(function(req, res)
            {
    // idCount = 0;
    // units = [];
    // players = [];
    // nbConnected = 0;

                var uri = url.parse(req.url).pathname;
                var index = uri.lastIndexOf('/');
                var str = uri.substr(index);

                if(uri === "/")
                  uri = "index.html";

                var filename = path.join("/client",uri);
                if(str === "/tartiflette.js")
                  filename = path.join("/server", uri);

    // console.log(filename);
    //console.log(mime.lookup(filename));

                if(mime.lookup(filename) === "image/png")
                {
                    fs.readFile(__dirname + filename, 'binary', function(error, content) {
                        if(error) {
                            res.writeHead(500);
                            return res.end("Error loading page");
                        }
                        res.setHeader('content-type', mime.lookup(filename));
                        res.write(content, "binary");
                        res.end();
                    });
                }
                else
                {
                    fs.readFile(__dirname + filename, 'utf-8', function(error, content) {
                        if(error) {
                            res.writeHead(500);
                            return res.end("Error loading page");
                        }
                        res.setHeader('content-type', mime.lookup(filename));
                        res.writeHead(200);
                        res.end(content);
                    });
                }
            });

        this.io = require('socket.io').listen(this.server);
    }

Server.prototype.initMaps = function(){
        this.maps =
            [{
                playerPosition : [100, 200],
                opponentPosition : [100, 1000],
                mapID : "map1"
            }];
    }

Server.prototype.setIOSockets = function(){
        var self = this;

        this.io.sockets.on('connection', function (socket,pseudo) {
            socket.on('pseudo', function(pseudo){
                self.nbConnected++;
                socket.pseudo = pseudo;
                socket.id = self.idPlayerCount;
                socket.ready = 0;

                self.players.push({pseudo : pseudo, id : self.idPlayerCount, ready : socket.ready});
                self.idPlayerCount++;

                socket.emit('playerId', {psuedo : pseudo, id : socket.id, players : self.players});
                socket.broadcast.emit("newConnection", self.players);

                if(self.inGame){
                    socket.emit("loadGame");
                    socket.emit("spectator");
                }

                console.log("Connexion de : "+ pseudo +" (id : "+ socket.id+")");
            });

            socket.on('newId', function(){
                socket.emit('newId', self.idCount++ );
            });

            socket.on('chatMessage', function(message){
                socket.broadcast.emit('chatMessage', socket.pseudo+" : "+message);
            });

            socket.on("disconnect", function(){
        var isPlayer = 0;
        var i = 0;

        self.nbConnected = Math.min(0, self.nbConnected - 1);

        for(i in self.players){
            if(self.players[i].id == socket.id){
                self.players[i].ready = 0;
                self.players.splice(i, 1);
            }
        }

        if(i < 2){
            self.ready = 0;
            isPlayer = 1;
        }

        socket.broadcast.emit("leaving", {pseudo : socket.pseudo, id : socket.id, playerReady : self.getPlayerReady()});

        socket.disconnect(); // destruction socket
    });

            socket.on('ready', function(){

        if(socket.ready == 1){
            self.ready = Math.min(0, self.ready - 1);
            socket.ready = 0;
        }
        else{
            self.ready++;
            socket.ready = 1; 

            if(self.ready >= 2){
                self.ready = 0;
                self.inGame = true;
                socket.broadcast.emit("loadGame");
                // self.socket1.emit('start', {name : self.socket1.pseudo, starting : self.maps[self.choosenMap].playerPosition});
                // self.socket1.emit('start', {name : self.socket2.pseudo, starting : self.maps[self.choosenMap].opponentPosition});
            }
        }  

        for(var i in self.players){
            if(self.players[i].id == socket.id){
                self.players[i].ready = socket.ready;
            }
        }

        socket.broadcast.emit('ready', {id : socket.id, playerReady : self.getPlayerReady()});
    });
            socket.on('gameInformation', function(entities){
        socket.entities = entities;
        socket.broadcast.emit("gameInformation",entities);

        // console.log(maps[0].playerPosition);
        // upload += sizeof(hihi);
    });
        });
    }

Server.prototype.startServer = function(){
        this.server.listen(8080);
    }

Server.prototype.getPlayerReady = function(){
    var playerReady = [];

    if(this.players[0]){
        playerReady[0] = this.players[0].ready;
    }
    else{
        playerReady[0] = 0;
    }

    if(this.players[1]){
        playerReady[1] = this.players[1].ready;
    }
    else{
        playerReady[1] = 0;
    }

    return playerReady;
}


var server = new Server();

server.startServer();