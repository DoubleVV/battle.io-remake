var init = function(){
	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

	// Application config
	// this.game = new Game;
	this.app = new Application;
	this.timeZero = Date.now();
	this.fps = 60;
	this.interval = 1000/fps;
	this.delta = 0;	
	this.now;

	this.spriteCanvas = document.getElementById('spriteCanvas');
    this.spriteContext = spriteCanvas.getContext('2d');
    this.mapCanvas = document.getElementById('mapCanvas');
    this.mapContext = mapCanvas.getContext('2d');
    // this.mapContext.setTransform(1, 0.5, -1, 0.5, 0, 0);
    // this.canvas = document.getElementById('hudCanvas');
    // this.context = canvas.getContext('2d');

    this.mapCanvas.width = window.innerWidth/2;
    this.mapCanvas.height = window.innerHeight;
    this.spriteCanvas.width = window.innerWidth/2;
    this.spriteCanvas.height = window.innerHeight;
    this.spriteCanvas.style.left = window.innerWidth/2 + "px";
    this.spriteCanvas.style.position = "absolute";

	// Game Config
	// this.game.init();
	// this.game.setSocket(socket);

	// window.setInterval(this.game.tick, 1000);
	// window.setInterval(this.game.client.update, 20);

	window.addEventListener('contextmenu', function(e) {
	    if (e.button === 2) {
	    	e.preventDefault();
	    	return false;
	    }
	}, false);

	window.onresize = function(event) {
    	this.mapCanvas.width = window.innerWidth/2;
    	this.mapCanvas.height = window.innerHeight;
    	this.mapCanvas.width = window.innerWidth/2;
    	this.mapCanvas.height = window.innerHeight;
    	this.spriteCanvas.style.left = window.innerWidth/2 + "px";

    	this.app.applySize(this.mapCanvas.width*2, this.mapCanvas.height);
	};
}

var run = function(){

	requestAnimationFrame(run);

	this.now = Date.now();

	this.delta = this.now - this.timeZero;

	if(this.delta > this.interval){
		this.timeZero = this.now - (this.interval);
		draw();
	}

	update();
}

var draw = function(){
	this.mapContext.clearRect(0, 0, this.mapCanvas.width, this.mapCanvas.height);
	this.spriteContext.clearRect(0, 0, this.spriteCanvas.width, this.spriteCanvas.height);
	// this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	this.app.draw();
    // this.game.draw(this.mapContext, this.context);
	// drawStats();
}

var drawStats = function(){
	this.mapContext.save();
	this.mapContext.fillStyle = 'white';
    this.mapContext.font = "10pt Arial";
    this.mapContext.fillText("FPS : " + Math.round(1000/this.delta), 30, 30);
    this.mapContext.restore();
}

var update = function(){
	// this.game.update(this.delta/1000);
	this.app.update(this.delta/1000);
}

window.onload = function(){
	init();
	run();
}

function loadJSON(file, callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', "maps/"+file+".json", true);
    xobj.onreadystatechange = function() {
        if (xobj.readyState == 4 && xobj.status == 200) {

            // .open will NOT return a value but simply returns undefined in async mode so use a callback
            callback({file : file ,map : xobj.responseText});

        }
    };
    xobj.send(null);
}

