class Player{
	constructor(){
	
    	this.ally = [];
	
    	// this.calculIncome();
	}

	isAlliedWith(player){
		for(var a in this.ally)
			if(player == this.ally[a])
				return true;
	
		return false;
	};
	
	setId(id){
		this.id = id;
	}
	
	addTown(){
		this.townCount++;
	}
	
	deleteTown(){
		this.townCount--;
	}

	setGame(game){
		this.game = game;
	}
}