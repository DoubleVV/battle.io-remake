class Client{

	constructor(app){
		this.socket = null;

		this.app = app;

		this.ping = 0;
		this.idPlayer = -1;

		this.currentId = 0;
		this.requestIdDone = false;

		this.launchGame = false;

		this.townInformation = [];
		this.unitInformation = [];
	}

	setSocket(socket){
		this.socket = socket;
		this.setOnSockets();
	}

	setOnSockets(){
		var self = this;

		this.socket.on('pong', function(data){
			self.ping = data;
		});

		this.socket.on('gameInformation', function(entities){
			// console.log(entities.town);

			for(var i in entities.town){
				if(entities.town[i]){

					if(!self.app.stateStack.stack[0].game.entities.town[entities.town[i].id]){
						new Town(entities.town[i]);
					}
					else{
						self.app.stateStack.stack[0].game.entities.town[entities.town[i].id].setInformation(entities.town[i]);
					}
				}
			}
			for(var i in entities.unit){
				if(entities.unit[i]){
					if(!self.app.stateStack.stack[0].game.entities.unit[entities.unit[i].id] && !entities.unit[i].dead){
						new Unit(entities.unit[i]);
					}
					else{
						self.app.stateStack.stack[0].game.entities.unit[entities.unit[i].id].setInformation(entities.unit[i]);
					}
				}
			}
			for(var i in entities.actionStack){
				if(entities.actionStack[i]){
					var action = Function(entities.actionStack[i].argument, entities.actionStack[i].methode);
	
					if(self.app.stateStack.stack[0].game.entities.town[entities.actionStack[i].id]){
						action(self.app.stateStack.stack[0].game.entities.town[entities.actionStack[i].id]);
					}
					if(self.app.stateStack.stack[0].game.entities.unit[entities.actionStack[i].id]){
						action(self.app.stateStack.stack[0].game.entities.unit[entities.actionStack[i].id]);

					}
				}	
			}
		});

		this.socket.on('start', function(data){
			console.log("starting in "+ data.starting[0]+", "+data.starting[1]);

			self.app.stateStack.stack[0].game.setPlayer(data.name, Types.Races.HUMAN, true, data.starting[0], data.starting[1]);

			self.app.stateStack.stack[0].game.startGame();
		});

		this.socket.on('opponentStart', function(data){
			self.app.stateStack.stack[0].game.setPlayer(data.name, Types.Races.ORC, false, data.starting[0], data.starting[1]);
		});

		this.socket.on('win', function(){
			self.app.stateStack.stack[0].game.paused = true;
			self.app.stateStack.stack[0].game.win = true;
		});

		this.socket.on('newId', function(data){
			console.log(self.app.stateStack.stack[0].game);
			self.app.stateStack.stack[0].game.waitingId[0].setId(data);
			self.app.stateStack.stack[0].game.waitingId.shift();
		});

		this.socket.on('spectator', function(){
			self.app.stateStack.stack[0].game.spectate = true;
			self.app.stateStack.stack[0].game.hasStarted = true;
		});

		this.socket.on('chatMessage', function(message){
			self.app.stateStack.makeCallback(function(state){
				state.chatBox.push(message);

				if(state.chatBox.length > 14){
					state.chatBox.shift();
				}
			});
		});

		this.socket.on('newConnection', function(players){
			self.app.stateStack.makeCallback(function(state){
				state.player = players;

				state.chatBox.push("Connection of "+players[players.length-1].pseudo);
				if(state.chatBox.length > 14){
					state.chatBox.shift();
				}
			});
		});

		this.socket.on('leaving', function(leaver){
			self.app.stateStack.makeCallback(function(state){
				var index = 0;

				for(index in state.player){
        		    if(state.player[index].id == leaver.id){
        		        state.player.splice(index, 1);
        		    }
        		}

				state.chatBox.push(leaver.pseudo+" has disconnected");
				if(state.chatBox.length > 14){
					state.chatBox.shift();
				}

				state.player[0].ready = leaver.playerReady[0];

				if(state.player[1]){
    				state.player[1].ready = leaver.playerReady[1];
				}

				state.buttons[1].unpress();
			});
		});

		this.socket.on('loadGame', function(){
			self.app.stateStack.makeCallback(function(state){
				state.requestStackPop();
				state.requestStackPush(stateID.LoadingState);
			});
		});

		this.socket.on('ready', function(data){
			self.app.stateStack.makeCallback(function(state){
				var index = 0;
    			state.player[0].ready = data.playerReady[0];
    			state.player[1].ready = data.playerReady[1];
			});
		});

		this.socket.on('playerId', function(data){
			self.idPlayer = data.id;
			self.pseudo = data.pseudo;
			self.app.stateStack.makeCallback(function(state){
				state.player = data.players;
				for(var i = 0; i < 2; i++){
					if(data.players[i]){
						state.player[i] = data.players[i].ready;
					}
				}
			});
			self.launchGame = true;
		});
	}

	sendChatMessage(message){
		this.socket.emit('chatMessage', message);
	}

	sendReady(bool){
		this.socket.emit('ready');
	}

	update(data){
	
	// toutes les 15 ms

		// if(self.app.stateStack.stack[0].game.spectate)
		// 	return;


		// this.townInformation = [];
		// this.unitInformation = [];

		// var tmp;

		// if(this.app.getCurrentStateID() == stateID.GameState){ 
		// 	for(var t in self.app.stateStack.stack[0].game.entities.town){
		// 		if(self.app.stateStack.stack[0].game.entities.town[t].isAllied()){
		// 			tmp = {id : self.app.stateStack.stack[0].game.entities.town[t].id,
		// 			hitPoints : self.app.stateStack.stack[0].game.entities.town[t].hitPoints,
		// 			maxHitPoints : self.app.stateStack.stack[0].game.entities.town[t].maxHitPoints,
		// 			player : self.app.stateStack.stack[0].game.entities.town[t].player,
		// 			posX : self.app.stateStack.stack[0].game.entities.town[t].posX,
		// 			posY : self.app.stateStack.stack[0].game.entities.town[t].posY,
		// 			width : self.app.stateStack.stack[0].game.entities.town[t].width,
		// 			height : self.app.stateStack.stack[0].game.entities.town[t].height,
		// 			name : self.app.stateStack.stack[0].game.entities.town[t].name,
		// 			stage : self.app.stateStack.stack[0].game.entities.town[t].stage,
		// 			dead : self.app.stateStack.stack[0].game.entities.town[t].dead,
		// 			};
		// 			this.townInformation.push(tmp);
		// 		}
		// 	}
		// 	for(var u in self.app.stateStack.stack[0].game.entities.unit){
		// 		if(self.app.stateStack.stack[0].game.entities.unit[u].isAllied()){
		// 			tmp = {id : self.app.stateStack.stack[0].game.entities.unit[u].id,
		// 			hitPoints : self.app.stateStack.stack[0].game.entities.unit[u].hitPoints,
		// 			maxHitPoints : self.app.stateStack.stack[0].game.entities.unit[u].maxHitPoints,
		// 			player : self.app.stateStack.stack[0].game.entities.unit[u].player,
		// 			state : self.app.stateStack.stack[0].game.entities.unit[u].state,
		// 			posX : self.app.stateStack.stack[0].game.entities.unit[u].posX,
		// 			posY : self.app.stateStack.stack[0].game.entities.unit[u].posY,
		// 			width : self.app.stateStack.stack[0].game.entities.unit[u].width,
		// 			height : self.app.stateStack.stack[0].game.entities.unit[u].height,
		// 			type : self.app.stateStack.stack[0].game.entities.unit[u].type,
		// 			orientation : self.app.stateStack.stack[0].game.entities.unit[u].orientation,
		// 			step : self.app.stateStack.stack[0].game.entities.unit[u].step,
		// 			anim : self.app.stateStack.stack[0].game.entities.unit[u].anim,
		// 			attackRange : self.app.stateStack.stack[0].game.entities.unit[u].attackRange,
		// 			visualRange : self.app.stateStack.stack[0].game.entities.unit[u].visualRange,
		// 			dead : self.app.stateStack.stack[0].game.entities.unit[u].dead,
		// 			};
		// 			this.unitInformation.push(tmp);
		// 		}
		// 	}
		// 	this.socket.emit('gameInformation', {town : this.townInformation, unit : this.unitInformation, actionStack : self.app.stateStack.stack[0].game.actionStack});
		// }

		// self.app.stateStack.stack[0].game.actionStack = [];
	}

	tick(){
		if(this.socket){
			this.socket.emit('stats', Date.now());
		}
	}

	requestNewId(){
		if(this.socket){
			this.socket.emit('newId');
		}
	}

	gameFinished(){
		this.socket.emit('lost');
	}
}