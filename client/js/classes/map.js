class Map{
	constructor(mapName, context){
		this.map = this.findMap(mapName, context);

		this.tileset = new Tileset(this.map.tilesets[0]);
		this.terrain = this.map.layers;
		this.walls = [];

		this.minimumTileSize = 64;

		this.tileHeight = (this.map.tileheight < this.minimumTileSize ? this.minimumTileSize : this.map.tileheight);
		this.tileWidth = (this.map.tilewidth < this.minimumTileSize ? this.minimumTileSize : this.map.tilewidth);

		this.height = this.map.height * this.tileHeight;
		this.width = this.map.width * this.tileWidth;
		
		this.getWalls();
		}
	
	getHeight() {
		return this.height;
	}
	getWidth() {
		return this.width;
	}

	draw(context, xView, yView){
		context.save();

		context.transform(  1,   0.5,
	                   -1,   0.5,
	                  0,   0    );

		for(var t in this.terrain){
			for(var h = 0; h < this.terrain[t].height; h++){
				for(var w = 0; w < this.terrain[t].width; w++){
					this.tileset.dessinerTile(this.terrain[t].data[(h * this.terrain[t].width) + w], context, w * this.tileWidth - xView - yView, h * this.tileHeight - yView + xView, this.tileWidth, this.tileHeight);
					// if(this.tileset.isCollidableTile(this.terrain[t].data[(h * this.terrain[t].width) + w])){
					// 	context.save();
					// 	context.fillStyle = "rgba(255, 255, 255, 1);"
					// 	context.fillRect(w * this.tileWidth - xView - yView, h * this.tileHeight - yView + xView, this.tileWidth + 1, this.tileHeight + 1);
					// 	context.restore();
					// }
				}
			} 
		}

		context.restore();
	}

	drawFlat(context, xView, yView, factor){
		context.save();

		var tileWidthWithFactor = this.tileWidth * factor;
		var tileHeightWithFactor = this.tileHeight * factor;

		for(var t in this.terrain){
			for(var h = 0; h < this.terrain[t].height; h++){
				for(var w = 0; w < this.terrain[t].width; w++){
					this.tileset.dessinerTile(this.terrain[t].data[(h * this.terrain[t].width) + w], context, w * tileWidthWithFactor, h * tileHeightWithFactor, tileWidthWithFactor, tileHeightWithFactor);
					// if(this.tileset.isCollidableTile(this.terrain[t].data[(h * this.terrain[t].width) + w])){
					// 	context.save();
					// 	context.fillStyle = "rgba(255, 255, 255, 1);"
					// 	context.fillRect(w * this.tileWidth - xView - yView, h * this.tileHeight - yView + xView, this.tileWidth + 1, this.tileHeight + 1);
					// 	context.restore();
					// }
				}
			} 
		}

		context.restore();
	}

	getWalls(){
		if(this.walls.length != 0){
			return this.walls;
		}
		else if(this.tileset.getTilesProperties().length != 0){
			for(var l in this.terrain){
				for(var t in this.terrain[l].data){
					this.walls[t] = this.tileset.isCollidableTile(this.terrain[l].data[t]);
				}
			}
		}
		else{
			return [];
		}
	}

	findMap(mapName, context){
		if(context.maps[mapName]){
			return context.maps[mapName];
		}
		return null;
	}
}