class GameState extends State{
	constructor(stack, context){
		super(stack, context);

		this.game = new Game("third", context);

		context.game = this.game;

		this.game.init();

		// A changer pour les soucis de sécurité
		window.setInterval(this.game.tick, 1000);
		window.setInterval(this.game.sendData, 1);

		this.state = stateID.GameState;
	}

	update(dt){
		this.game.update(dt);

		this.dt = dt;

		return true;
	}

	draw(){
		this.game.draw(this.mapCanvas.getContext('2d'));
		this.game.drawDebug(this.testCanvas.getContext('2d'));

		mapContext.save();
		mapContext.fillStyle = 'white';
	    mapContext.font = "72pt Arial";
	    // mapContext.fillText(1/this.dt, 20, 100);
	   	mapContext.restore();
	}

	handleEvent(event){
		this.game.processHudInput(event);
		this.game.processGameInput(event);
		return true;
	}

	makeCallback(callback){
		callback(this);
		return true;
	}

	applySize(w, h){
		this.game.applySize(w, h);
	}
}