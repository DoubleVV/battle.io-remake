class Town extends Entity{
	constructor(player, kind, posX, posY, context, camera){
		super(player, kind, posX, posY, context, camera);

		this.init();
	}

	draw(context, xView, yView){
		super.draw(context, xView, yView);
		var screenPosition = this.getScreenPosition(xView, yView);


		var texturePath = "";
		var overlay;

		if(this.kind[0] == Types.Races.HUMAN){
			texturePath += "human_bat";
		}
		else if(this.kind[0] == Types.Races.ORC){
			texturePath += "orc_bat";
		}
		else{
			texturePath += "human_bat";
		}

		texturePath += this.batImg;

		if(this.selected && !(this.client.idPlayer == this.idPlayer)){
			overlay = '_pink';
		}
		else if(this.selected){
			overlay = '_green';
		}
		else if(!(this.idPlayer == this.client.idPlayer)){
			overlay = '_red';
		}
		else{
			overlay = '_';
		}

		texturePath += overlay;
		
		context.drawImage(this.texture[texturePath], 0, 0, this.width, this.height, screenPosition.x, screenPosition.y, this.width, this.height);		
	}

	init(){
		this.grade = Towns.niveau[0].grade;
		this.description = Towns.niveau[0].description;
		this.production = Towns.niveau[0].production;
		this.cout = Towns.niveau[0].cout;
		this.resources = Towns.niveau[0].apports;
		this.healthPoint = Towns.niveau[0].points_vie;
		this.availableConstruction = Towns.niveau[0].emplacements_construction;
		this.upgradeTime = Towns.niveau[0].temps_upgrade;
		this.batImg = Towns.niveau[0].bat_img;

		this.stage = 0;
		this.width = 142;
		this.height = 132;
	}

	update(delta){
		super.update();
	}
}