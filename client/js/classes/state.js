class State{
	constructor(stack, context){
		this.stack = stack;
		this.context = context;

		this.mapCanvas = document.getElementById('mapCanvas');
		this.testCanvas = document.getElementById('spriteCanvas');

		this.player = [];

		this.chatInput = "";
		this.chatBox = [];

		this.state = stateID.None;
	}

	requestStackPush(state){
		this.stack.pushState(state);
	}

	requestStackPop(){
		this.stack.popState();
	}

	requestStackClear(){
		this.stack.clearStates();
	}

	getContext(){
		return this.context;
	}

	makeCallback(callback){
		callback(this);
	}

	applySize(w, h){
		return;
	}
}