class HudState extends State{
	constructor(stack, context){
		super(stack, context);

		this.setGame(context.game);

		this.state = stateID.HudState;
	}

	setGame(game){
		this.game = game;

		this.initInterface();
	}

	initInterface(){
		self = this;

		this.container = new Container();

		var buttonTest3 = new Button(self.game.context, self.game.camera);
		buttonTest3.setPosition(100,500);
		buttonTest3.setSize(100, 40)
		buttonTest3.setText("Spawn unitées");
		buttonTest3.setCallback(function(){
			// self.game.test.push(new Unit(self.game.context.client.idPlayer, kinds.knight, 5*25, 50, self.game.context, self.game.camera));

			for(var i = 0; i < 100; i++){
				// self.game.test.push(new Unit(self.game.context.client.idPlayer, kinds.archer, getRandomInt(640), getRandomInt(640), self.game.context, self.game.camera));
				self.game.test.push(new Unit(self.game.context.client.idPlayer, kinds.knight, 5+i%20*25, Math.floor(i/20)*50, self.game.context, self.game.camera));
				// self.game.test.push(new Unit(self.game.context.client.idPlayer, kinds.knight, getRandomInt(640), getRandomInt(640), self.game.context, self.game.camera));
				// self.game.test.push(new Unit(0, kinds.archer, getRandomInt(640), getRandomInt(640), self.game.context, self.game.camera));
				self.game.test.push(new Unit(100, kinds.archer, 150+i%20*25, 450+Math.floor(i/20)*50, self.game.context, self.game.camera));
				// self.game.test.push(new Unit(0, kinds.knight, getRandomInt(640), getRandomInt(640), self.game.context, self.game.camera));
			}
			console.log("LE PLAN A ECHOUEEEEEEEEEEEE");
		});

		var buttonTest4 = new Button(self.game.context);
		buttonTest4.setPosition(100,450);
		buttonTest4.setText("Tuer toutes les unitées");
		buttonTest4.setSize(100, 40)
		buttonTest4.setCallback(function(){
			for(var t in self.game.test){
				if(!self.game.test[t].isDead()){
					self.game.test[t].die();
				}
			}
		});

		var buttonTest5 = new Button(self.game.context);
		buttonTest5.setPosition(100,400);
		buttonTest5.setText("Animation \"attaque\"");
		buttonTest5.setSize(220, 40)
		buttonTest5.setCallback(function(){
			for(var t in self.game.test){
				self.game.test[t].attack();
			}
		});

		var buttonTest6 = new Button(self.game.context);
		buttonTest6.setPosition(100,550);
		buttonTest6.setText("-100hp");
		buttonTest6.setCallback(function(){
			for(var t in self.game.test){
				self.game.test[t].hurt(100);
			}
		});

		this.container.pack(buttonTest3);
		this.container.pack(buttonTest4);
		this.container.pack(buttonTest5);
		this.container.pack(buttonTest6);
	}

	update(dt){
		return true;
	}

	draw(){
		this.container.draw(mapContext);
	}

	handleEvent(event){
		this.container.handleEvent(event);
		return false;
	}
}