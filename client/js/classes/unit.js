class Unit extends Entity{
	constructor(player, kind, posX, posY, context, camera){
		super(player, kind, posX, posY, context, camera);

		this.nextDestination = null;
		this.path =	[];

		this.animatedSprite = new animatedSprite(0.3);
		this.animations = [];
		this.orientation = Types.Orientations.DOWN;

		this.opponents = [];

		// A E S T H E T I C S
		this.showRangeZone = false;
		this.showCombatZone = false;

		this.testRangeBox = false;
		this.testCombatBox = false;

		this.init();
	}

	draw(context){
		super.draw(context);

		var screenPosition = this.getScreenPosition(this.camera.posX, this.camera.posY);

		var suffix = "";

		if(this.selected && !(this.client.idPlayer == this.idPlayer)){
			suffix = 'pink';
		}
		else if(this.selected){
			suffix = 'green';
		}
		else if(!(this.idPlayer == this.client.idPlayer)){
			suffix = 'red';
		}

		this.drawZones(context);

		this.animatedSprite.draw(context, this.texture, suffix, screenPosition.x, screenPosition.y);

		// context.drawImage(this.texture[this.texturePrefix+"image_walk_"], 0, 0, this.width, this.height, screenPosition.x, screenPosition.y, this.width, this.height);		
	}

	drawFlat(context, factor){
		context.save();	

		// Unit

		if(this.selected && !(this.client.idPlayer == this.idPlayer)){
			context.fillStyle = 'pink';
			context.fillRect(this.posX * factor, this.posY * factor, this.width * 0.5 * factor , this.height * 0.5 * factor);
		}
		else if(this.selected){
			context.fillStyle = 'green';
			context.fillRect(this.posX * factor, this.posY * factor, this.width * 0.5 * factor, this.height * 0.5 * factor);
		}
		else if(!(this.idPlayer == this.client.idPlayer)){
			context.fillStyle = 'red';
			context.fillRect(this.posX * factor, this.posY * factor, this.width * 0.5 * factor, this.height * 0.5 * factor);
		}
		else{
			context.fillStyle = 'blue';
			context.fillRect(this.posX * factor, this.posY * factor, this.width * 0.5 * factor, this.height * 0.5 * factor);
		}

		// Combat Zones

		var combatZone = this.getCombatZone();

		context.strokeStyle = "orange";
		context.strokeRect(combatZone.x * factor, combatZone.y * factor, combatZone.w * factor, combatZone.h * factor);

		// Range Zones

		var rangeZone = this.getRangeZone();

		context.strokeStyle = "white";
		context.strokeRect(rangeZone.x * factor, rangeZone.y * factor, rangeZone.w * factor, rangeZone.h * factor);
	
		context.restore();
	}

	drawZones(context){
		if(this.dead){
			return;
		}

		this.drawRangeZone(context);
		this.drawCombatZone(context);
	}

	drawRangeZone(context){
		if(!this.showRangeZone){
			return;
		}

		context.save();

		var rangeZone = this.getRangeZone(this.camera.posX, this.camera.posY);

		context.strokeStyle = "white";

		// rangeZone.x = rangeZone.x - rangeZone.y - this.camera.posX * 2 
		// rangeZone.y = rangeZone.x * 0.5 + rangeZone.y * 0.5 - this.camera.posY 
		// rangeZone.w = rangeZone.w * 0.5 + rangeZone.h
		// rangeZone.h = rangeZone.w * 0.5 + rangeZone.h

		// context.beginPath();
		// context.moveTo(rangeZone.x, rangeZone.y);
		// context.lineTo(rangeZone.x + (rangeZone.w + rangeZone.h), rangeZone.y);
		// context.lineTo(rangeZone.x + (rangeZone.w + rangeZone.h), rangeZone.y + (rangeZone.w*0.5 + rangeZone.h*0.5));
		// context.lineTo(rangeZone.x, rangeZone.y + (rangeZone.w*0.5 + rangeZone.h*0.5));
		// context.lineTo(rangeZone.x, rangeZone.h);
		// context.stroke();

		context.strokeRect(rangeZone.x, rangeZone.y, rangeZone.w, rangeZone.h);
	
		context.restore();
	}

	drawCombatZone(context){
		if(!this.showCombatZone){
			return;
		}

		context.save();	
	
		var combatZone = this.getCombatZone();

		if(this.testRangeBox){
			context.strokeStyle = "orange";
		}
		else{
			context.strokeStyle = 'yellow';
		}

		context.strokeRect(combatZone.x, combatZone.y, combatZone.w, combatZone.h);
	
		context.restore();
	
	}

	setAttackRate(rate){
		this.attackCooldown = new Timer(rate);
	}

	init(){
		this.texturePrefix = this.getType();

		this.img_height = Unites[this.texturePrefix]["image"]["walk"].img_size_x;
		this.img_width = Unites[this.texturePrefix]["image"]["walk"].img_size_y;
		this.width = Unites[this.texturePrefix]["image"]["walk"].sprite_size_x;
		this.height = Unites[this.texturePrefix]["image"]["walk"].sprite_size_y;
		this.anim_max = Unites[this.texturePrefix]["image"]["walk"].nb_anim -1;
			
	
		this.speed = Unites[this.texturePrefix]["vitesse_deplacement"];
		this.atk_speed = Unites[this.texturePrefix]["vitesse_attaque"];
		
		this.setMaxHitPoints(Unites[this.texturePrefix]["points_vie"]);
		this.hitPoints = this.maxHitPoints;
		
		this.setAttackRate(300);
		this.attackRange = Unites[this.texturePrefix]["portee_attaque"];
		this.visualRange = Unites[this.texturePrefix]["champs_de_vision"];
		this.attackDamage = Unites[this.texturePrefix]["force"];

		this.initAnimation();
	}

	update(delta){
		super.update(delta);

		this.updateAnimation(delta);

		if(this.dead){
			return;
		}

		this.removeDeadOpponents();

		this.checkAttack();

		if(this.attacking){
			return;
		}

		this.followOpponent();

		var oldposX =  this.posX;
		var oldposY =  this.posY;

		this.move(delta);

		if (oldposX == this.posX && oldposY == this.posY){
			if(!this.attacking){
				this.animatedSprite.setAnimation(this.animations["standing"][this.orientation], true);
				this.animatedSprite.setLooped(true);
			}
			return;		
		}

		this.findOrientation(oldposX, oldposY);
	}

	move(delta){
		var center = this.getCenter();
		if(this.following)
			this.follow();
		if(this.nextDestination != null && !this.attacking){
			if(length(center.x - this.nextDestination.x, center.y - this.nextDestination.y) > 4){
				this.moveTo(this.nextDestination.x, this.nextDestination.y, delta);
			}
			else{	
				if(this.path.length > 0){
					var nextInPath = this.path.shift();
					this.nextDestination = {x : nextInPath.x, y : nextInPath.y};
				}
				else{
					this.nextDestination = null;
				}
			}
		}
	}

	moveTo(x, y, delta){
	
		var center = this.getCenter();
	
		var directionVector = unitVector(x - center.x, y - center.y);
		
		this.posX += Math.round(directionVector.vx * this.speed * delta);
		this.posY += Math.round(directionVector.vy * this.speed * delta);
	}

	setDestination(x, y){
		this.nextDestination = {x : x, y : y};
	}

	initAnimation(){
		for(var animationInfo in Unites[this.getType()]["image"]){
			this.animations[animationInfo] = [];
			
			for(var orientation in Types.Orientations){
				var animationFrame = new Animation();

				animationFrame.setSpriteSheet(this.texturePrefix+"image_"+animationInfo+"_");
				for(var f = 0; f < Unites[this.getType()]["image"][animationInfo].img_size_x/this.width; f++){
					animationFrame.addFrame({x : f*this.width, y : Types.Orientations[orientation]*this.height, w : this.width, h : this.height});
				}

				this.animations[animationInfo].push(animationFrame);
			}

		}
		
		this.animations["standing"] = [];

		for(var orientation in Types.Orientations){
			var animationFrame = new Animation();

			animationFrame.setSpriteSheet(this.texturePrefix+"image_walk_");
			animationFrame.addFrame({x : 0, y : Types.Orientations[orientation]*this.height, w : this.width, h : this.height});

			this.animations["standing"].push(animationFrame);
		}

		this.animatedSprite.setAnimation(this.animations["standing"][this.orientation], true);

		this.animatedSprite.play();

	}

	getType(){
		var type = "";

		if(this.kind[0] == Types.Races.ORC){
			type += "orc";
		}

		switch(this.kind[1]){
			case Types.Entities.WARRIOR :
				type += "sword";
				break;
			case Types.Entities.ARCHER :
				type += "archer";
				break;
			case Types.Entities.KNIGHT :
				type +="knight";
				break;
			default :
				type += "archer";
				break;
		}

		return type;
	}

	updateAnimation(delta){
		this.animatedSprite.update(delta);
	}

	die(){
		super.die();

		switch(this.orientation){
			case Types.Orientations.DOWN :
				this.orientation = Types.Orientations.DOWNLEFT;
				break;
			case Types.Orientations.LEFT :
				this.orientation = Types.Orientations.UPLEFT;
				break;
			case Types.Orientations.RIGHT :
				this.orientation = Types.Orientations.DOWNRIGHT;
				break;
			case Types.Orientations.UP :
				this.orientation = Types.Orientations.UPRIGHT;
				break;
			default :
				break;
		}

		this.animatedSprite.setAnimation(this.animations["death"][this.orientation], true);
		this.animatedSprite.setFrameTime(0.6);
		this.animatedSprite.setLooped(false);
	}

	attack(){

		if(this.dead){
			return;
		}

		if(!this.attackCooldown.isOver(Date.now())){
			return;
		}

		this.attacking = true;

		if(this.target){
			this.target.hurt(this.attackDamage);
		}

		this.animatedSprite.setAnimation(this.animations["hit"][this.orientation], true);
		this.animatedSprite.setLooped(false);
	}

	getOrientation(){
		return this.orientation;
	}

	findOrientation(oldposX, oldposY){
		var angle = Math.atan2(this.posX - oldposX, this.posY - oldposY);

		// Converts from radians to degrees.
		var angle_conv = (angle * 180 / Math.PI)+45;
	
		if (angle_conv>180){
			angle_conv = angle_conv -360;
		}
	
			
		if (angle_conv > 0){
			if (angle_conv < 90){
				if (angle_conv < 23){
					this.orientation = Types.Orientations.LEFT;
				}
				else if (angle_conv < 68){
					this.orientation = Types.Orientations.DOWNLEFT;
				}
				else{
					this.orientation = Types.Orientations.DOWN;
				}
			}
			else{
				if (angle_conv < 113){
					this.orientation = Types.Orientations.DOWN;
				}
				else if (angle_conv < 156){
					this.orientation = Types.Orientations.DOWNRIGHT;
				}
				else {
					this.orientation = Types.Orientations.RIGHT;
				}
			}
		}
		else{
			if (angle_conv > -90){
				if (angle_conv > -23){
					this.orientation = Types.Orientations.LEFT;
				}
				else if (angle_conv > -68){
					this.orientation = Types.Orientations.UPLEFT;
				}
				else{
					this.orientation = Types.Orientations.UP;
				}
			}
			else{
				if (angle_conv > -113){
					this.orientation = Types.Orientations.UP;
				}
				else if (angle_conv > -156){
					this.orientation = Types.Orientations.UPRIGHT;
				}
				else {
					this.orientation = Types.Orientations.RIGHT;
				}
			}
		}
		//done

		this.animatedSprite.setAnimation(this.animations["walk"][this.orientation], false);
	}

	checkAttack(){
		if(this.attacking){
			if(this.target){
				if(this.target.dead){
					this.disengage();
				}
			}
			
			if(this.attackCooldown.isOver(Date.now())){
				this.attacking = false;
				this.animatedSprite.play();
			}
		}
	}

	getCombatZoneOnScreen(){
		var center = this.getCenterOnScreen(this.camera.posX, this.camera.posY);
	
		var rect = {x : center.x - this.attackRange*32/2,
					y : center.y - this.attackRange*32/2,	
					w : this.attackRange*32,
					h : this.attackRange*32};
	
		return rect;
	}

	getCombatZone(){
		var center = this.getCenter();
	
		var rect = {x : center.x - this.attackRange*32/2,
					y : center.y - this.attackRange*32/2,	
					w : this.attackRange*32,
					h : this.attackRange*32};
	
		return rect;
	}

	follow(){
		var targetPos = this.getTargetPos();
		this.setDestination(targetPos.x, targetPos.y);

		if(collisionBox(this.getCombatZoneOnScreen(), this.target.getSizeOnScreen())){
			this.testRangeBox = true;
			this.attack();
			this.nextDestination = null;
		}
		else{
			this.testRangeBox = false;
		}
	}
	
	getTargetPos(){
	
		if(this.target){
			return this.target.getCenter();
		}
	
		return {};
	}

	engage(entity){
		if(!this.nextDestination){
			this.setFollow(entity);
		}
	}
	
	disengage(entity){
		this.attacking = false;
		this.following = false;
		this.removeTarget();
	}
	
	setFollow(entity){
		this.following = true;
		this.setTarget(entity);
	}

	removeTarget(){
		this.target = null;
		this.nextDestination = null;
	}

	setTarget(entity){
		this.target = entity;
	}

	onEnnemyInRange(ennemy){
		if(this.opponents.includes(ennemy)){
			return;
		}

		this.addOpponent(ennemy);
	}

	addOpponent(opponent){
		this.opponents.push(opponent);
	}

	clearOpponent(){
		this.opponents = [];
	}

	followOpponent(){
		if(this.opponents.length == 0){
			return;
		}

		var closestOpponent = this.opponents[0];
		var distance = length(this.opponents[0].posX, this.opponents[0].posY);

		for(var o in this.opponents){
			if(length(this.opponents[o].posX, this.opponents[o].posY) < distance){
				closestOpponent = this.opponents[o];
				distance = length(this.opponents[o].posX, this.opponents[o].posY);
				// console.log(distance);
			}
		}

		this.setFollow(closestOpponent);
	}

	removeDeadOpponents(){
		for(var o in this.opponents){
			if(this.opponents[o].dead){
				this.opponents.splice(o, 1);
			}
		}
	}
}