class Tileset{
	constructor(data){
		// Chargement de l'image dans l'attribut image
		this.image = new Image();
		this.image.referenceDuTileset = this;
		this.tiles = [];
		this.image.onload = function() {
			if(!this.complete) 
				throw new Error("Erreur de chargement du tileset nommé \"" + url + "\".");
	
			// Largeur du tileset en tiles
			this.referenceDuTileset.largeur = this.width / 32;

			this.referenceDuTileset.loadTiles(data.tileproperties);
		}
		// this.image.src = "tilesets/" + url;
		this.image.src = data.image;
	}

	dessinerTile(numero, context, xDestination, yDestination, tileWidthSize, tileHeigthSize) {
		var xSourceEnTiles = numero % this.largeur;
		if(xSourceEnTiles == 0) 
			xSourceEnTiles = this.largeur;
	
		var ySourceEnTiles = Math.ceil(numero / this.largeur);
	
		var xSource = (xSourceEnTiles - 1) * 32;
		var ySource = (ySourceEnTiles - 1) * 32;
	
		context.drawImage(this.image, xSource, ySource, 32, 32, xDestination, yDestination, tileWidthSize + 1, tileHeigthSize + 1);
	}

	loadTiles(tileProperties){
		for(var t in tileProperties){
			this.tiles[t] = tileProperties[t].Collision;
		}
	}

	getTilesProperties(){
		return this.tiles;
	}

	isCollidableTile(tile){
		return this.tiles[tile-1];
	}
}