class Rect{
	constructor(posX, posY, width, height){
		this.posX = posX || 0;
		this.posY = posY || 0;
		this.width = width || 0;
		this.height = height || 0;

		// A E S T H E T I C
		this.show = true;
	}

	draw(context, color){
		if(!this.show){
			return;
		}

		context.save();

		context.strokeStyle = color || "black";
		context.strokeRect(this.posX, this.posY, this.width, this.height);

		context.restore();
	}
}