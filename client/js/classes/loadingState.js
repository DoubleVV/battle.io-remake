class LoadingState extends State{
	constructor(stack, context){
		super(stack, context);

		this.maps = context.maps;
		this.texture = context.texture;

		this.loadMaps();
		this.loadSprites();

		this.pourcentage = 0;

		this.state = stateID.LoadingState;

		this.client = context.client;

		this.test = false;
	}

	update(dt){
		this.dt = dt;

		this.checkIfReady();


		return true;
	}

	draw(){
		// TODO : Change ça !

		mapContext.save();
		mapContext.fillStyle = 'white';
	    mapContext.font = "72pt Arial";
	    mapContext.fillText("Loading", 20, 100);
	    mapContext.fillText(this.pourcentage * 100 +"%", 20, 180);
	   	mapContext.restore();
	}

	handleEvent(event){
		return true;
	}

	makeCallback(callback){
		callback(this);
		return true;
	}

	loadMaps(){
		var self = this;

		for(var m in mapName){

			loadJSON(mapName[m], function(reponse){
				var mapReponse = JSON.parse(reponse.map);

				// self.maps.push({name : reponse.file, map : mapReponse});
				self.maps[reponse.file] = mapReponse;

				self.checkIfReady();		
			});
		}
	}

	loadSprites(){
		this.loadTownSprites();
		this.loadUnitSprites();
	}

	loadTownSprites(){
		var initPath = "./sprites/jeu/";

		var idConstruction = 0;

		for(var t in Towns.niveau){
			if(idConstruction != Towns["niveau"][t].bat_img){
				idConstruction = Towns["niveau"][t].bat_img;

				for(var i = 0; i < 4; i++){
					var overlay;

					switch(i){
						case 0 : 
							overlay = "_pink";
							break;
						case 1 :
							overlay = "_green";
							break;
						case 2 :
							overlay = "_red";
							break;
						default :
							overlay = "_";
					}

					for(var j in Types.Races){
						var sprite = new Image();
						var race;

						switch(j){
							case "HUMAN" : 
								race = "human";
								break;
							case "ORC" :
								race = "orc";
								break;
							default :
								race = "human";
								break;
						}

						sprite.src = initPath+"batiments/"+race+"/bat_"+idConstruction+overlay+".png";

						var idTexture = race+"_bat"+Towns["niveau"][t].bat_img+overlay;
						
						this.texture[idTexture] = sprite;
					}
				}
			}
		}
	}

	loadUnitSprites(){
		for(var i in Unites){

			if(Unites[i]["image"]){
				for(var j = 0; j < 4; j++){
					var sprite = new Image();

					var overlay = "";

					switch(j){
						case 0 :
							overlay = "_";
							break; 
						case 1 : 
							overlay = "_green";
							break;
						case 2 :
							overlay = "_red";
							break;
						case 3 :
							overlay = "_pink"
							break;
					}

					sprite.src = Unites[i]["image"]["walk"]["anim_path"]+overlay+".png";

					var idTexture = i+"image_walk"+overlay;

					this.texture[idTexture] = sprite;
				}
			}
			if(Unites[i]["image"]){
				for(var j = 0; j < 4; j++){
					var sprite = new Image();

					var overlay = "";

					switch(j){
						case 0 :
							overlay = "_";
							break; 
						case 1 : 
							overlay = "_green";
							break;
						case 2 :
							overlay = "_red";
							break;
						case 3 :
							overlay = "_pink"
							break;
					}

					sprite.src = Unites[i]["image"]["death"]["anim_path"]+overlay+".png";

					var idTexture = i+"image_death"+overlay;

					this.texture[idTexture] = sprite;
				}
			}
			if(Unites[i]["image"]){
				for(var j = 0; j < 4; j++){
					var sprite = new Image();

					var overlay = "";

					switch(j){
						case 0 :
							overlay = "_";
							break; 
						case 1 : 
							overlay = "_green";
							break;
						case 2 :
							overlay = "_red";
							break;
						case 3 :
							overlay = "_pink"
							break;
					}

					sprite.src = Unites[i]["image"]["hit"]["anim_path"]+overlay+".png";

					var idTexture = i+"image_hit"+overlay;

					this.texture[idTexture] = sprite;
				}
			}
		}
	}

	checkIfReady(){

		this.checkClientData();

		this.pourcentage = (Object.keys(this.maps).length + 12)/(mapName.length + 12);
		// this.pourcentage = this.timer.pourcentageOver(Date.now());

		if(this.pourcentage >= 1 && this.test){
			this.finish();
		}

		return false;
	}

	finish(){
		this.requestStackPop();
		this.requestStackPush(stateID.GameState);
		this.requestStackPush(stateID.HudState);
	}

	checkClientData(){
		this.test = this.client.launchGame;
	}
}