class Component{
	constructor(){
		this.isSelected = false;
		this.isActive = false;
	}

	isSelected(){
		return this.isSelected;
	}

	select(){
		this.isSelected = true;
	}

	deselect(){
		this.isSelected = false;
	}

	isActive(){
		return this.isActive;
	}

	activate(){
		this.isActive = true;
	}

	deactivate(){
		this.isActive = false;
	}

	handleEvent(event){
		
	}
}