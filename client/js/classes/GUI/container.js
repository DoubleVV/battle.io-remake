class Container extends Component{
	constructor(){
		super();

		this.children = [];
		this.selectedChild = -1;
	}

	pack(component){
		this.children.push(component);
		if(this.hasSelection() && component.isSelectable())
			select(this.children.length-1);
	}

	isSelectable(){
		return false;
	}

	handleEvent(event){
		for(var child in this.children){
			this.children[child].handleEvent(event);
		}
	}

	hasSelection(){
		return this.selectedChild >=0;
	}

	select(index){
		if(this.children[this.index].isSelectable()){
			if(this.hasSelection())
				this.children[this.selectedChild].deselect();
			this.children[this.index].select();
			this.selectedChild = this.index;
		}
	}

	selectNext(){
		if(!this.hasSelection())
			return;
		var next = this.selectedChild;
		do
			next = (next + 1) % this.children.length;
		while (!this.children[next].isSelectable());
		this.select(next);
	}

	selectPrevious(){
		if(!this.hasSelection())
			return;
		var prev = this.selectedChild;

		do
			prev = (prev + this.children.length-1) % this.children.length;
		while(!this.children[prev].isSelectable());
		this.select(prev);
	}

	draw(canvasContext){
		for(var child in this.children){
			this.children[child].draw(canvasContext);
		}
	}

}