class Button extends Component{
	constructor(context){
		super();

		this.type = {
			NORMAL : 1,
			SELECTED : 2,
			PRESSED : 3
		}

		this.sprite = null;
		this.text = "";

		this.callback = null;

		this.isToggle = true;

		this.posX = 0;
		this.posY = 0;
		this.width = 60;
		this.height = 40;

		this.changeTexture(this.type.NORMAL);
	}

	isSelectable(){
		return true;
	}

	isActive(){
		super.isActive();
	}

	select(){
		super.select();
		this.changeTexture(this.type.SELECTED);
	}

	deselect(){
		super.deselect();
		this.changeTexture(this.type.NORMAL)
	}

	activate(){
		super.activate();
		if(this.isToggle)
			this.changeTexture(this.type.PRESSED);
		if(this.callback)
			this.callback();
		if(!this.isToggle)
			this.deactivate();
	}

	deactivate(){
		super.deactivate();
		if(this.isToggle){
			if(super.isSelected())
				this.changeTexture(this.type.SELECTED);
			else
				this.changeTexture(this.type.NORMAL);
		}
	}

	handleEvent(event){
		switch(event.type){
			case "mousedown" :
				if(event.which == 1){
					if(super.isSelected()){
						this.activate();
					}
				}
				break;
			case "mouseup" :
				if(event.which == 1){
					this.deactivate();
				}
				break;
			case "mousemove" :
				if(collisionBox(this.getBox(), {x : event.pageX, y : event.pageY, w : 0, h : 0})){
					if(!super.isSelected()){
						this.select();
					}
				}
				else{
					if(super.isSelected()){
						this.deselect();
					}
				}
				break;
		}
	}

	draw(canvasContext){
		canvasContext.save();

		switch(this.state){
			case this.type.NORMAL :
				canvasContext.fillStyle = 'yellow';
				break;
			case this.type.SELECTED :
				canvasContext.fillStyle = 'orange';
				break;
			case this.type.PRESSED :
				canvasContext.fillStyle = 'purple';
				break;
			default :
				canvasContext.fillStyle = 'black';
				break;
		}

		canvasContext.fillRect(this.posX, this.posY, this.width, this.height);
		canvasContext.fillStyle = 'black';
		canvasContext.font = "8pt Arial";
		canvasContext.fillText(this.text, this.posX+5, this.posY+this.height/2-4);

		canvasContext.restore();
	}

	setText(text){
		this.text = text;
	}

	setToggle(flag){
		this.isToggle = flag;
	}

	changeTexture(buttonType){
		this.state = buttonType;
	}

	setCallback(callback){
		this.callback = callback;
	}

	setPosition(x, y){
		this.posX = x;
		this.posY = y;
	}

	getBox(){
		return {x : this.posX,
				y : this.posY,
				w : this.width,
				h : this.height};
	}

	setSize(x, y){
		this.width = x;
		this.height = y;
	}
}