class Game{
	constructor(mapName, context, spectate){
		this.context = context;

		this.client = context.client;

		// Setting up the Map
		this.map = new Map(mapName, context);

		// Setting up the input system
		this.mouseObj = new MouseObj;

		// Setting up the Camera
		this.camera = new Camera(0, 0, window.innerWidth, window.innerHeight, this.map.getWidth(), this.map.getHeight());

		// Setting up our HUD
		// this.hud = new Hud(this);

		// Setting up the layers
		this.layer;

		// Setting up our entities
		this.entities = [];

		// QuadTree of the scene
		this.quadTree = new QuadTree({x : 0, y : 0, width : this.map.getWidth(), height : this.map.getHeight()}, false, 10, 2);

		// Pausing 
		this.paused = true;
		this.hasStarted = false;
		this.lost = false;
		this.spectate = spectate || true;

		this.test(context);

		this.init();
	}

	init(){
		this.checkCombatTimer = new Timer(100);
		this.checkRemoveWreck = new Timer(1000);
	}

	disableContextMenu(){
		
	}
	
	addEventListenerToCanvas(canvas, callback){
		
	}
	
	update(delta){
		var self = this;

		this.quadTree.clear();

		this.forEachEntityTest(function(entity){
			var combatZone = entity.getCombatZone();
			var rangeZone = entity.getRangeZone();

			self.quadTree.insert({x : entity.posX, y : entity.posY, width : entity.width, height : entity.height});
			self.quadTree.insert({x : combatZone.x, y : combatZone.y, width : combatZone.w, height : combatZone.h});
			self.quadTree.insert({x : rangeZone.x, y : rangeZone.y, width : rangeZone.w, height : rangeZone.h});	
		});

		this.updateTest(delta);
		this.handleCollision();

		this.removeWreck();

		this.camera.update(delta, this.mouseObj.outLeft, 
								  this.mouseObj.outTop, 
								  this.mouseObj.outRight,
								  this.mouseObj.outBottom);
		// this.hud.update(delta);
	}

	drawDebug(testContext){
		var tileWidth = this.map.tileWidth;
		var tileHeight = this.map.tileHeight;

		var factor = 0.5;

		var self = this;

		this.map.drawFlat(testContext, this.camera.posX, this.camera.posY, factor);
		this.forEachEntityTest(function(entity){
			entity.drawFlat(testContext, factor);
		});

		this.drawNode(testContext, factor, this.quadTree.root);
	}

	drawNode(testContext, factor, node){
		testContext.save();	

		var bounds = node._bounds;

		testContext.strokeStyle = 'black';

		testContext.strokeRect(
				(this.abs(bounds.x)  + 0.5) * factor,
				(this.abs(bounds.y) + 0.5) * factor,
				bounds.width * factor,
				bounds.height * factor
			);
		
		var len = node.nodes.length;
		
		for(var i = 0; i < len; i++)
		{
			this.drawNode(testContext, factor, node.nodes[i]);
		}

		testContext.restore();	
	}

	//fast Math.abs
	abs(x){
		return (x < 0 ? -x : x);
	}
	
	draw(mapContext) {
		this.drawOnMapContext(mapContext);
	}
	
	drawOnMapContext(mapContext){
		this.map.draw(mapContext, this.camera.posX, this.camera.posY);
		this.drawTest(mapContext);
		this.mouseObj.draw(mapContext, this.camera.posX, this.camera.posY);
		// this.hud.draw(mapContext);		
	}
	
	drawOnHudContext(hudContext){

	}
	
	processHudInput(event){
		// this.hud.handleEvent(event);
	}
	
	processGameInput(event){
		switch(event.type){
			case "mousedown" : 
				this.mouseObj.startX = event.pageX;
				this.mouseObj.startY = event.pageY;
				this.mouseObj.mouseX = event.pageX;
				this.mouseObj.mouseY = event.pageY;
				if(event.which == 1){
					this.mouseObj.setIsDrawing(true, this.camera.posX, this.camera.posY);
				}
				break;	
			case "mouseup" : 
				if(event.which == 1){
					if(this.mouseObj.mouseY < this.camera.hView){
						this.handleLeftClick(this.mouseObj.getSelectionBox(this.camera.posX, this.camera.posY));
					}
				}
				else if(event.which == 3){
					this.handleRightClick(this.mouseObj.mouseX, 
					this.mouseObj.mouseY);
				}
	
				this.mouseObj.setIsDrawing(false);
				this.mouseObj.startX = event.pageX;
				this.mouseObj.startY = event.pageY;
				
				break;
			case "mousemove" :
				this.mouseObj.mouseX = event.pageX;
				this.mouseObj.mouseY = event.pageY;
				this.handleMouseMove(this.mouseObj.mouseX, 
						this.mouseObj.mouseY);
				break;
		}
	}
	
	handleLeftClick(selectionBox){
		var entityBox = {x : 0, y : 0, w : 0, h : 0};
	
		// this.selectedEntities.town = null;
		this.selectedEntities = [];
	
		var self = this;
		this.forEachEntityTest(function(entity){
			entityBox = entity.getScreenPosition(self.camera.posX, self.camera.posY);
			entityBox.w = entity.width;
			entityBox.h = entity.height;
	
			if(collisionBox(selectionBox, entityBox)){
				self.selectedEntities.push(entity);	
				entity.select();	
			}
			else{
				entity.unselect();
			}
		});
	}
	
	handleRightClick(x, y){
		var click = this.convertClickPosition(x, y);

		var self = this;

		this.forEachSelectedEntity(function(entity){
			if(entity.getKind()[2] == "unit" && entity.isAllied()){
				entity.disengage();
				entity.setDestination(click.x, click.y);
				for(var u in self.test){
					if(self.test[u] != entity && !self.test[u].isDead()){
						if(!self.test[u].isAlliedWith(entity)){
							if(collisionBox({x : x, y : y, w : 0, h : 0}, self.test[u].getSizeOnScreen(self.camera.posX, self.camera.posY))){
								entity.setFollow(self.test[u]);
							}
						}
					}
				}
			}
		});
	}

	convertClickPosition(xClick, yClick){
		return {x : xClick*0.5 + yClick + this.camera.posX + this.camera.posY, y : xClick*-0.5 + yClick + this.camera.posY - this.camera.posX};
	}
	
	handleMouseMove(x, y){
		if(x < 50)
			this.mouseObj.setOutLeft(true);
		else
			this.mouseObj.setOutLeft(false);
	
		if(x > this.camera.wView - 50)
			this.mouseObj.setOutRight(true);
		else
			this.mouseObj.setOutRight(false);
	
		if(y < 50)
			this.mouseObj.setOutTop(true);
		else
			this.mouseObj.setOutTop(false);
	
		if(y > this.camera.hView - 50)
			this.mouseObj.setOutBottom(true);
		else
			this.mouseObj.setOutBottom(false);
	}
	
	handleCollision(){
		if(this.checkCombatTimer.isOver(Date.now())){
			this.checkCombatZone();
		}
	}
	
	checkCombatZone(){
		for(var e1 in this.test){

			if(this.test[e1].getKind()[2] != "unit"){
				continue;
			}

			this.test[e1].clearOpponent();
			for(var e2 in this.test){
				if(e1 == e2){
					continue;
				}

				if(this.test[e1].isAlliedWith(this.test[e2])){
					continue;
				}

				if(this.test[e1].dead || this.test[e2].dead){
					continue;
				}

				if(this.test[e2].getKind()[2] != "unit"){
					continue;
				}				

				if(!collisionBox(this.test[e1].getRangeZoneOnScreen(), this.test[e2].getSizeOnScreen())){
					continue;
				}

				this.test[e1].onEnnemyInRange(this.test[e2]);				
			}
		}
	}

	forEachEntity(callback){	

	}
	
	forEachTown(callback){	

	}
	
	forEachUnit(callback){	

	}

	tick(){

	}

	setSocket(socket){

	}

	getNewId(entity){

	}

	startGame(){

	}

	setPlayer(name, type, allied, startingX, startingY, context){

	}

	sendError(message){

	}

	removeWreck(){
		for(var i in this.test){
			if(this.test[i].isWreck()){
				this.test.splice(i, 1);
			}
		}
	}

	sendData(){
		if((this.paused || this.hasStarted) && !this.spectate){
			var data = this.makeNetworkData();

			this.client.update(data);
		}
	}

	makeNetworkData(){
		return "This is a test";
	}

	applySize(w, h){
		this.camera.wView = w;
		this.camera.hView = h;
	}

	test(context){
		//Test
		this.test = [];

		// this.test.push(new Town(context.client.idPlayer, kinds.town, getRandomInt(this.map.getWidth()), getRandomInt(this.map.getHeight()), context, this.camera));
		// this.test.push(new Town(1, Types.Races.ORC, getRandomInt(this.map.getWidth()), getRandomInt(this.map.getHeight()), context, this.camera));
		// this.test.push(new Unit(context.client.idPlayer, kinds.archer, 50, 50, context, this.camera));
		// this.test.push(new Unit(context.client.idPlayer, kinds.knight, 50, 100, context, this.camera));
	}

	drawTest(mapContext){
		var self = this;

		this.forEachEntityTest(function(entity){
			entity.draw(mapContext, self.camera.posX, self.camera.posY);
		});
	}

	updateTest(delta){
		this.forEachEntityTest(function(entity){
			entity.update(delta);
		});
	}

	forEachEntityTest(callback){	
		for(var i in this.test){
			callback(this.test[i]);
		}
	}

	forEachSelectedEntity(callback){
		for(var e in this.selectedEntities){
			callback(this.selectedEntities[e]);
		}
	}
}
