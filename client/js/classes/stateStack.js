class StateStack{
	constructor(texture){
		this.stack = [];
		this.pendingList = [];

		this.texture = texture;
		this.factories = [];
	}

	update(dt){
		for(var i in this.stack){
			if(!this.stack[i].update(dt))
				break;
		}

		this.applyPendingChanges();
	}

	draw(){
		for(var i in this.stack){
			this.stack[i].draw();
		}
	}

	handleEvent(event){
		for(var i in this.stack){
			if(!this.stack[i].handleEvent(event))
				break;
		}
	}

	pushState(state){
		var self = this;
		this.pendingList.push({action : function(){
			for(var i in self.factories){
				if(i == state){
					self.stack.push(self.factories[i]());
					return;
				}
			}
		}});
	}

	makeCallback(callback){
		for(var i in this.stack){
			if(!this.stack[i].makeCallback(callback))
				break;
		}
	}

	popState(){
		var self = this;
		this.pendingList.push({action : function(){self.stack.pop();}})
	}

	clearStates(){
		var self = this;
		this.pendingList.push({action : function(){self.stack = [];}})
	}

	isEmpty(){
		return (this.stack.length == 0);
	}

	applyPendingChanges(){
		for(var i in this.pendingList){
			this.pendingList[i].action();
		}

		this.pendingList = [];
	}

	registerState(state, callback){
		this.factories[state] = callback;
	}

	getCurrentStateID(){
		if(this.stack == []){
			return null;
		}

		return this.stack[0].state;
	}

	applySize(w, h){
		for(var i in this.stack){
			this.stack[i].applySize(w, h);
		}
	}
}