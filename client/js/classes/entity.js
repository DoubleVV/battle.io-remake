class Entity{
	constructor(player, kind, posX, posY, context, camera){
		this.selected = false;

		this.idPlayer = player;
		this.client = context.client;

		this.posX = posX;
		this.posY = posY;

		this.kind = kind;
		this.dead = false;

		this.texture = context.texture;
		this.camera = camera;

		this.isMarkedForRemoval = false;

		// A E S T H E T I C
		this.showHealthBar = true;
		this.showSelectionRect = false;
		this.showHPVariation = false;

		this.HPVariation = [];
		this.HPTimer = [];
	}

	copy(entity){
		var copy = new Entity(entity.player, entity.kind, entity.posX, entity.posY, entity.context);

		copy.kind = entity.kind;
		copy.id = entity.id;

		// Position
		copy.posX = entity.posX;
		copy.posY = entity.posY;
		copy.width = entity.width;
		copy.height = entity.height;

		// Range
		copy.visualRange = entity.visualRange;
		copy.attackRange = entity.attackRange;
		
		// Movement
		copy.nextDestination = entity.nextDestination;
		
		// Health
		copy.hitPoints = entity.hitPoints;
		copy.maxHitPoints = entity.maxHitPoints;

		return copy;
	}

	draw(context){		
		var screenPosition = this.getScreenPosition(this.camera.posX, this.camera.posY);

		if(this.dead){
			return;
		}

		this.drawSelectionRect(context, screenPosition);
		this.drawHealthBar(context, screenPosition);
		this.drawHPVariation(context, screenPosition);
	}

	drawSelectionRect(context, screenPosition){

		if(!this.showSelectionRect){
			return;
		}

		context.save();

		if(this.selected && !(this.client.idPlayer == this.idPlayer)){
			context.strokeStyle = 'pink';
			context.strokeRect(screenPosition.x, screenPosition.y, this.width, this.height);
		}
		else if(this.selected){
			context.strokeStyle = 'green';
			context.strokeRect(screenPosition.x, screenPosition.y, this.width, this.height);
		}
		else if(!(this.idPlayer == this.client.idPlayer)){
			context.strokeStyle = 'red';
			context.strokeRect(screenPosition.x, screenPosition.y, this.width, this.height);
		}
		else{
			context.strokeStyle = 'blue';
			context.strokeRect(screenPosition.x, screenPosition.y, this.width, this.height);
		}

		context.restore();
	}

	drawHealthBar(context, screenPosition){
		if(!this.showHealthBar){
			return;
		}

		var hitPointPourcentage = this.hitPoints/this.maxHitPoints;
		var widthBar = this.width*hitPointPourcentage;
		var centerBar = this.width/2;

		context.save();

		if(hitPointPourcentage > 0.75){
			context.fillStyle = "green";
			context.fillRect(screenPosition.x, screenPosition.y - 20, widthBar, 10);		
		}
		else if(hitPointPourcentage > 0.30){
			context.fillStyle = "yellow";
			context.fillRect(screenPosition.x, screenPosition.y - 20, widthBar, 10);		
		}
		else{
			context.fillStyle = "red";
			context.fillRect(screenPosition.x, screenPosition.y - 20, widthBar, 10);
		}

		context.strokeStyle = "black";
		context.strokeRect(screenPosition.x, screenPosition.y - 20, this.width, 10);

		context.fillStyle = 'black';
		context.font = "7pt Arial";
		context.fillText(this.hitPoints+"/"+this.maxHitPoints, screenPosition.x + centerBar/2, screenPosition.y - 11);		

		context.restore();
	}

	drawHPVariation(context, screenPosition){

		if(!this.showHPVariation){
			return;
		}

		for(var i in this.HPTimer){
			var timePourcent = this.HPTimer[i].timer.pourcentageOver(Date.now());

			if(timePourcent > 1){
				delete this.HPTimer[i];
				return;
			}

			context.save();

			if(this.HPTimer[i].value > 0){
				context.fillStyle = "green";
			}
			else{
				context.fillStyle = "red";
			}

			context.font = "7pt Arial";
			context.fillText(this.HPTimer[i].value, position.x + this.width/2, position.y-50*timePourcent);
			
			context.restore();
		}
	}

	update(delta){
		// this.updateHPVariation();
		if(this.dead){
			if(this.deathTimer.isOver(Date.now())){
				this.markForRemoval();
			}
		}


	}

	updateHPVariation(){

		if(this.HPVariation.length == 0){
			return;
		}

		var sum = 0;

		for(var hpv in this.HPVariation){
			sum += this.HPVariation[hpv];
		}

		// console.log(this.HPVariation);

		this.HPVariation = [];

		this.HPTimer.push({value : sum, timer : new Timer(1000)});

		// console.log(this.HPTimer);


	}

	select(){
		this.selected = true;
	}
	
	unselect(){
		this.selected = false;
	}

	isAlliedWith(entity){
		if(this.idPlayer == entity.idPlayer){
			return true;
		}
	
		return false;
	}

	die(){
		this.dead = true;
	
		if(this.deathCallback) {
			this.deathCallback();
	    }

		this.deathTimer = new Timer(3000, Date.now());
	}

	getCenter(){
		var width = this.width/4 + this.height/2;
		var height = -this.width/4 + this.height/2;

		return {x : this.posX + width, y : this.posY + height};
	}
	
	getSize(){
		var position = this.getCenter();
	
		var box = {x : position.x, y : position.y, w : this.width, h : this.height};	
		return box;
	}

	getSizeOnScreen(){
	
		var position = this.getScreenPosition(this.camera.posX, this.camera.posY);
	
		var box = {x : position.x, y : position.y, w : this.width, h : this.height};	
		return box;
	}

	getCenterOnScreen(){

		var center = this.getScreenPosition(this.camera.posX, this.camera.posY);
		center.x += this.width/2;
		center.y += this.height/2;
		
		return center;
	}

	getRangeZoneOnScreen(){
		var center = this.getCenterOnScreen(this.camera.posX, this.camera.posY);
	
		var rect = {x : center.x - (this.visualRange+1)*32/2,
					y : center.y - (this.visualRange+1)*32/2,	
					w : (this.visualRange+1)*32,
					h : (this.visualRange+1)*32};
	
		return rect;
	}

	getRangeZone(){
		var center = this.getCenter();
	
		var rect = {x : center.x - (this.visualRange+1)*32/2,
					y : center.y - (this.visualRange+1)*32/2,	
					w : (this.visualRange+1)*32,
					h : (this.visualRange+1)*32};
	
		return rect;
	}

	getScreenPosition(){
		return {x : this.posX + this.posY * -1 - this.camera.posX * 2, y : this.posX * 0.5 + this.posY*0.5 - this.camera.posY};
	}

	getKind(){
		return this.kind;
	}

	isDead(){
		return this.dead;
	}

	setMaxHitPoints(hp){
		this.maxHitPoints = hp;
		this.hitPoints = hp;
	}

	hurt(dmg){
		if(this.dead){
			return;
		}

		this.hitPoints -= dmg;
		this.HPVariation.push(-dmg);
		if(this.hitPoints <= 0){
			this.die();
		}	
	}

	isAlliedWith(entity){
		return (this.idPlayer == entity.idPlayer);
	}

	isAllied(){
		return (this.client.idPlayer == this.idPlayer);
	}

	markForRemoval(){
		this.isMarkedForRemoval = true;
	}

	isWreck(){
		return this.isMarkedForRemoval;
	}
}