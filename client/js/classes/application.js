class Application{
	constructor(){
		this.stateStack = new StateStack();
		this.context = {
			player : new Player(),
			client : new Client(this),
			texture : [],
			maps : []
		};

		this.context.client.setSocket(socket);

		var self = this;

		window.addEventListener('mousedown', function(event){ self.handleEvent(event);}, true);
		window.addEventListener('mouseup', function(event){ self.handleEvent(event);}, true);
		window.addEventListener('mousemove', function(event){ self.handleEvent(event);}, true);
		window.addEventListener('keydown', function(event){ self.handleEvent(event);}, true);

		this.registerStates();
		this.stateStack.pushState(stateID.LoadingState);
	}

	update(dt){
		this.stateStack.update(dt);
	}

	draw(){
		this.stateStack.draw();
	}

	handleEvent(event){
		this.stateStack.handleEvent(event);
	}

	registerStates(){
		var self = this;
		this.stateStack.registerState(stateID.GameState, function(){
			return new GameState(self.stateStack, self.context);
		})

		this.stateStack.registerState(stateID.RoomState, function(){
			return new RoomState(self.stateStack, self.context);
		})

		this.stateStack.registerState(stateID.LoadingState, function(){
			return new LoadingState(self.stateStack, self.context);
		})
		this.stateStack.registerState(stateID.HudState, function(){
			return new HudState(self.stateStack, self.context);
		})
	}

	getCurrentStateID(){
		return this.stateStack.getCurrentStateID();
	}

	applySize(w, h){
		this.stateStack.applySize(w, h);
	}
}