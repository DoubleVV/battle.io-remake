class animatedSprite{
	constructor(frameTime, paused, looped){
		this.frameTime = frameTime || 0.1;
		this.paused = paused || false;
		this.looped = looped || true;

		this.currentFrame = 0;
		this.currentTime = 0;
		this.animation;
		this.texture;

		this.rect = {x : 0, y : 0, w : 0, h : 0};
	}

	setAnimation(animation, reset){
		this.animation = animation;
		if(this.animation){
			this.texture = this.animation.getSpriteSheet();
		}
		if(reset){
			this.currentFrame = 0;
		}
		this.setFrame(this.currentFrame);
	}

	setFrameTime(time){
		this.frameTime = time;
	}

	play(){
		this.paused = false;
	}

	stop(){
		this.paused = true;
		this.currentFrame = 0;
		this.setFrame(this.currentFrame);
	}

	setLooped(looped){
		this.looped = looped;
	}

	getAnimation(){
		return this.animation;
	}

	isLooped(){
		return this.looped;
	}

	isPlaying(){
		return !this.looped;
	}

	getFrameTime(){
		return this.frameTime;
	}

	setFrame(newFrame){
		if(this.animation){
			this.rect = this.animation.getFrame(newFrame);
		}
	}

	update(delta){
		if(!this.paused && this.animation){

			this.currentTime += delta;

			if(this.currentTime >= this.frameTime){

				this.currentTime = this.currentTime%this.frameTime;

				if(this.currentFrame + 1 < this.animation.getSize()){
					this.currentFrame++;
				}
				else{
					if(!this.looped){
						this.paused = true;
					}
					else{
						this.currentFrame = 0;
					}
				}

				this.setFrame(this.currentFrame);
			}
		}
	}

	draw(canvasContext, texture, suffix, xPos, yPos){
		// console.log(texture);
		if(this.animation){
			canvasContext.drawImage(texture[this.texture+suffix], this.rect.x, this.rect.y, this.rect.w, this.rect.h, xPos, yPos, this.rect.w, this.rect.h);
		}
	}

	isLastFrame(){
		// console.log(this.currentFrame+1 == this.animation.getSize());
		// console.log(this.currentTime >= this.frameTime);

		// return (this.currentFrame+1 == this.animation.getSize() && this.currentTime >= this.frameTime);
		return (this.currentFrame+1 == this.animation.getSize());
	}
}