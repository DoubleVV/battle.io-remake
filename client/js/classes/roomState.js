class RoomState extends State{
	constructor(stack, context){
		super(stack, context);
		var self = this;

		this.buttons = [];

	   	this.setupButtons();

	   	this.state = stateID.RoomState;
	}

	update(dt){
		this.dt = dt;

		return false;
	}

	draw(){
		for(var i in this.buttons){
			this.buttons[i].draw(mapContext);
		}

	   	mapContext.save();
	   	mapContext.fillStyle = 'white';
	    mapContext.font = "20pt Arial";

	    if(this.player[0]){
	   		mapContext.fillText("player 1 : "+this.player[0].pseudo+"("+this.player[0].ready+")", 20, window.innerHeight - 135);
	    }
	    
	    if(this.player[1]){
	    	mapContext.fillText("player 2 : "+this.player[1].pseudo+"("+this.player[1].ready+")", 20, window.innerHeight - 100);
	    }

	    mapContext.fillText("spectator : ", 500, 700);
	    for(var i = 2; i < this.player.length; i++){
	    	if(this.player[i].id == this.context.client.idPlayer){
	    		mapContext.save();
	   			mapContext.fillStyle = 'yellow';
	    		mapContext.fillText(this.player[i].pseudo, 500, 665 + i*35);
	    		mapContext.restore();
	    		continue;
	    	}
	    	mapContext.fillText(this.player[i].pseudo, 500, 665 + i*35);
	    }
	   	mapContext.restore();

	   	mapContext.save();
	   	mapContext.fillStyle = 'grey';
	   	mapContext.fillRect(20, 20, window.innerWidth - 40, 500);
	   	mapContext.fillRect(20, 550, window.innerWidth - 260, 70);
	   	this.buttons[0].setPosition(window.innerWidth - 220, 550);
	   	this.buttons[1].setPosition(window.innerWidth - 220, 650);
	   	// mapContext.fillRect(window.innerWidth - 220, 550, 200, 70);
	    mapContext.font = "16pt Arial";
	   	mapContext.fillStyle = 'white';
	    mapContext.fillText("Envoyer", window.innerWidth - 160, 590);
	   	mapContext.restore();

	   	mapContext.save();
	   	mapContext.font = "16pt Arial";
	   	mapContext.fillStyle = 'black';
	   	for(var i in this.chatBox){
	    	mapContext.fillText(this.chatBox[i], 30, 45 + i*35);
	    }
	    mapContext.fillText(this.chatInput, 30, 590);
	    mapContext.restore();

	 //    mapContext.save();
		// mapContext.fillStyle = 'white';
	 //    mapContext.font = "72pt Arial";
	 //    mapContext.fillText(this.dt, 20, 100);
	 //   	mapContext.restore();
	}

	handleEvent(event){
		if(event.type == "keydown"){
			if(event.keyCode > 64 && event.keyCode < 91 || event.keyCode > 47 && event.keyCode < 58 || event.keyCode > 95 && event.keyCode < 112 || event.keyCode > 185 && event.keyCode < 221 || event.keyCode == 223 || event.keyCode == 226 || event.keyCode == 222 || event.keyCode == 32){
				this.chatInput = this.chatInput+event.key;
			}
			else if(event.keyCode == 8){
				this.chatInput = this.chatInput.slice(0, -1);
			}
			else if(event.keyCode == 13){
				this.context.client.sendChatMessage(this.chatInput);
				this.chatInput = "";
			}

		}

		if(event.type == "mousedown"){
			if(event.which == 1){
				for(var i in this.buttons){
					if(this.buttons[i].showButton && collisionBox({x : event.offsetX, y : event.offsetY, w : 0, h : 0}, this.buttons[i].getBoundingBox())){
            	        this.buttons[i].press();
            	    }
				}
			}
		}

		if(event.type == "mouseup"){
			for(var i in this.buttons){
				this.buttons[i].unpress();
			}
		}

		return false;
	}

	makeCallback(callback){
		super.makeCallback(callback);
		this.checkButtons();
		return false;
	}

	setupButtons(){
		var self = this;

		var button = new Button("", window.innerWidth - 220, 550, null, 200, 70, true);

	   	button.onClick = function(){
	   		self.context.client.sendChatMessage(self.chatInput);
			self.chatInput = "";
	   	};
		this.buttons.push(button);


	   	// var isPlayer = player.indexOf(leaver.pseudo);

	   	button = new Button("Prêt", window.innerWidth - 220, 650, null, 200, 70, false, true)

		button.onClick = function(){
	   		self.context.client.sendReady(true);
	   	};

	   	button.onUnclick = function(){
	   		self.context.client.sendReady(false);
	   	};
	   	// if(this.player[0].id == this.context.client.idPlayer || this.player[1].id == this.context.client.idPlayer)

	   	this.buttons.push(button);
	}

	checkButtons(){
		this.buttons[1].showButton = false;

		if(this.player[1])
			if(this.player[0].id == this.context.client.idPlayer || this.player[1].id == this.context.client.idPlayer){
	   			this.buttons[1].showButton = true;
		}
	}
}