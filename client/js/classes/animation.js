class Animation{
	constructor(){
		this.texture;
		this.frames = [];
	}

	addFrame(rect){
		this.frames.push(rect);
	}

	setSpriteSheet(texture){
		this.texture = texture;
	}

	getSpriteSheet(){
		return this.texture;
	}

	getSize(){
		return this.frames.length;
	}

	getFrame(index){
		return this.frames[index];
	}
}