
Types = {
    Messages: {
    },
    
    Entities: {

        // Units
        WARRIOR: 1,
        ARCHER: 2,
        KNIGHT: 3,

        ORCWARRIOR: 4,
        ORCARCHER: 5,
        ORCKNIGHT: 6,

        // Squads
        SQUAD: 7,

        //Town
        TOWN: 8,
        ORCTOWN: 9

    },

    Races: {
        HUMAN: 1,
        ORC: 2
    },
    
    Orientations: {
        UP: 0,
        UPLEFT : 1,
        UPRIGHT : 2,
        LEFT: 3,
        RIGHT : 4,
        DOWNLEFT : 5,
        DOWNRIGHT : 6,
        DOWN: 7
    }
};

var kinds = {
    warrior: [Types.Races.HUMAN, Types.Entities.WARRIOR, "unit"],
    archer: [Types.Races.HUMAN, Types.Entities.ARCHER, "unit"],
    knight: [Types.Races.HUMAN, Types.Entities.KNIGHT, "unit"],

    orcwarrior: [Types.Races.orc, Types.Entities.WARRIOR, "unit"],
    orcarcher: [Types.Races.orc, Types.Entities.ARCHER, "unit"],
    orcknight: [Types.Races.orc, Types.Entities.KNIGHT, "unit"],

    squad: [Types.Entities.SQUAD, "squad"],

    town: [Types.Races.HUMAN, Types.Entities.TOWN, "town"],
    orctown: [Types.Races.ORC, Types.Entities.TOWN, "town"],
};

var stateID = {
    None : 0,
    RoomState : 1,
    GameState : 2,
    LoadingState : 3,
    HudState : 4
}

var mapName = [
    "premiere",
    "second",
    "third",
    "fourth"
];